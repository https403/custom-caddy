package main

import (
	"github.com/caddyserver/caddy/caddy/caddymain"

	// DNS Provider
	_ "github.com/caddyserver/dnsproviders/cloudflare"
	_ "github.com/caddyserver/dnsproviders/digitalocean"
	_ "github.com/caddyserver/dnsproviders/dyn"
	_ "github.com/caddyserver/dnsproviders/googlecloud"
	_ "github.com/caddyserver/dnsproviders/namecheap"
	_ "github.com/caddyserver/dnsproviders/ovh"
	_ "github.com/caddyserver/dnsproviders/route53"
	_ "github.com/caddyserver/dnsproviders/vultr"

	// Middleware
	_ "github.com/linkonoid/caddy-dyndns"
	_ "github.com/nicolasazrak/caddy-cache"
	_ "github.com/captncraig/cors"
	_ "github.com/epicagency/caddy-expires"
	_ "github.com/BTBurke/caddy-jwt"
	_ "github.com/tarent/loginsrv/caddy"
	_ "github.com/hacdias/caddy-minify"
	_ "github.com/xuqingfeng/caddy-rate-limit"
	_ "github.com/captncraig/caddy-realip"
)

func main() {
	caddymain.EnableTelemetry = false
	caddymain.Run()
}