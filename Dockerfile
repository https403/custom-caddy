FROM golang:1.13-alpine as builder

WORKDIR /go/src/app
COPY . .

ENV GO111MODULE=on
RUN apk add --no-cache git
RUN go get -d -v ./...
RUN go install -v ./...

FROM alpine:3.8

COPY --from=builder /go/bin/caddy /usr/bin/caddy

ENTRYPOINT ["caddy"]