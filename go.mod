module caddy

go 1.12

require (
	github.com/BTBurke/caddy-jwt v3.7.1+incompatible
	github.com/caddyserver/caddy v1.0.3
	github.com/caddyserver/dnsproviders v0.3.0
	github.com/captncraig/caddy-realip v0.0.0-20190710144553-6df827e22ab8
	github.com/captncraig/cors v0.0.0-20190703115713-e80254a89df1
	github.com/epicagency/caddy-expires v1.1.1
	github.com/hacdias/caddy-minify v1.0.2
	github.com/linkonoid/caddy-dyndns v0.0.0-20190718171622-2414d6236b0f
	github.com/nicolasazrak/caddy-cache v0.3.4
	github.com/pquerna/cachecontrol v0.0.0-20180517163645-1555304b9b35 // indirect
	github.com/tarent/loginsrv v1.3.1
	github.com/xuqingfeng/caddy-rate-limit v1.6.6
)

replace github.com/h2non/gock => gopkg.in/h2non/gock.v1 v1.0.14
